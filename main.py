from src.ts_conf import TSConf
from src.ts_conversion import TSconversion
from src.ts_dft import TSdft
from src.tools import get_multiplicity, remove_unnecessary_files
import pathlib, glob

dir_base = pathlib.Path(__file__).parent.absolute()

############### Input settings ###############
ts = "AAr.xyz"  # transition state in xyz format
dft_level = "b3lyp/3-21g"  # dft level in format {method}/{basis set}
charge = 0  # charge of the transition state
atoms = [5,11]  # atoms to be fixed in format [atom1,atom2,atom3,...]
max_conformers = 2  # maximum number conformers to be considered in dft calculations
rwth_project = "rwth0687"  # valid rwth computing project
crest_processors = 12  # number of processors to run the crest calculations
gaussian_processors = 12  # number of processors to run the gaussian calculations
##############################################

# Get the multiplicity of ts
multiplicity = get_multiplicity(ts_xyz=ts)

# CREST calculations
TSConf(ts=ts, charge=charge, atoms=atoms, multiplicity=multiplicity,
       crest_processors=crest_processors, rwth_project=rwth_project).run_crest()

# Conversion: CREST to Gaussian input files
TSconversion(crest_conformers="crest_conformers.xyz", charge=charge, multiplicity=multiplicity,
             max_conformers=max_conformers, gaussian_processors=gaussian_processors, dft_level=dft_level).crest2gjf()

# Submitting DFT calculation with Gaussian
gjf = glob.glob('conformer*.gjf')

for conformer in gjf:
   TSdft(ts=conformer, rwth_project=rwth_project, gaussian_processors=gaussian_processors).dft_run()

# Remove unnecessary files
remove_unnecessary_files()

