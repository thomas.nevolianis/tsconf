from setuptools import setup, find_packages

setup(
    name='tsconf',
    version='1.0.0',
    author='Thomas Nevolianis',
    author_email='thomas.nevolianis@rwth-aachen.de',
    description="Transition state conformers",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: 3-Clause BSD License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'numpy >= 1.19.5'
    ]
)
