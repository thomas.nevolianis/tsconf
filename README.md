# TSconf 
-------------------

The TSconf tool is a trial for searching of transition states. The tool has been tested to two systems and thus, no results are promised.

## Features
--------------------
* Transition state conformers at b3lyp/tzvp
* Fast calculations


## Installation
--------------------
python3 setup.py install --user (automatically with the ./run.sh)
 

## Usage
--------------------
* Open the ```main.py``` and edit the input section:
* (The multiplicity of the given transition state is calculated automatically by using openbabel)

```python

############### Input settings ###############
ts = "AAr.xyz"  # transition state in xyz format
dft_level = "b3lyp/3-21g"  # dft level in format {method}/{basis set}
charge = 0  # charge of the transition state
atoms = [5,11]  # atoms to be fixed in format [atom1,atom2,atom3,...]
max_conformers = 2  # maximum number conformers to be considered in dft calculations
rwth_project = "rwth0687"  # valid rwth computing project
crest_processors = 12  # number of processors to run the crest calculations
gaussian_processors = 12  # number of processors to run the gaussian calculations
##############################################

```

* Open a terminal and run:
    ./tsconf.sh

