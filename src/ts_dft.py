# This source code is part of the TSconf package and is distributed
# under the 3-Clause BSD License. Please see 'LICENSE.rst' for further
# information.

__name__ = "ts_dft"
__author__ = "Thomas Nevolianis"
__all__ = ["TSdft"]

import pathlib
from shutil import copy2
import subprocess


class TSdft(object):
    """
    It creates the job bash files for SLURM and starts the DFT calculations.

    Parameters
    ----------
    ts : gjf format file : transition state conformer
    rwth_project : str : rwth computing project
    """

    def __init__(self, ts, rwth_project, gaussian_processors, work_directory):
        self.ts = ts
        self.ts_name = pathlib.Path(self.ts).stem
        self.gaussian_processors = gaussian_processors
        self.rwth_project = rwth_project
        self.work_directory = work_directory

    def create_ts_dir(self):
        """
        Creates the TS folder in which the calculations run.
        """
        tmp_dir = pathlib.Path(".", self.work_directory)
        tmp_dir.mkdir(exist_ok=True, parents=True)  # it's fine if it already exists
        return tmp_dir

    def create_dft_tmp(self):
        """
        Creates a dft temporary folder to write and perform the gaussian calculations.
        """
        tmp_dir = pathlib.Path(".", self.create_ts_dir() / f"dft_{self.ts_name}_tmp")
        tmp_dir.mkdir(exist_ok=True, parents=True)  # it's fine if it already exists
        return tmp_dir

    def copy_dft_to_dir(self, destination: pathlib.Path = None):
        """
        Copies the transition state conformer to the related folder.
        """
        if destination is None:
            destination = self.create_dft_tmp()
        copy2(pathlib.Path(self.ts), destination)

    def create_job_file(self, file_path: pathlib.Path = None):
        """
        It creates the job bash file that is used with SLURM to submit the gaussian calculations.
        """
        if file_path is None:
            file_path = self.create_dft_tmp() / "job_gaussian.sh"

        with open(file_path, 'w') as f2:
            f2.write('#!/bin/bash\n')
            f2.write(f'#SBATCH -J {self.ts_name}\n')
            f2.write('#SBATCH -e log.err.%j\n')
            f2.write('#SBATCH -o log.out.%j\n')
            f2.write('#SBATCH -t 20:00:00\n')
            f2.write(f'#SBATCH -n {self.gaussian_processors}\n')
            f2.write('#SBATCH --mem=48G\n')
            f2.write(f'#SBATCH --account={self.rwth_project}\n\n')
            f2.write('module load CHEMISTRY gaussian/16.b01_bin\n')
            f2.write(f"g16 < {self.ts_name}.gjf > {self.ts_name}.log")

    def dft_run(self):
        """
        It submits the gaussian calculation to the rwth cluster using SLURM
        """
        work_directory = self.create_dft_tmp()
        self.copy_dft_to_dir(work_directory)
        job_script_path = work_directory / "job_gaussian.sh"
        self.create_job_file(job_script_path)
        try:
            p = subprocess.run(f"sbatch < job_gaussian.sh",
                               cwd=f'{work_directory}',
                               check=True,
                               shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            raise ValueError(f"Gaussian failed with errorcode {e.returncode}"
                             f" and stderr: {e.stderr}")
        else:
            with open(work_directory / 'gaussian.stdout', 'w') as out:
                out.write(p.stdout.decode())


