# This source code is part of the TSconf package and is distributed
# under the 3-Clause BSD License. Please see 'LICENSE.rst' for further
# information.

__name__ = "ts_conversion"
__author__ = "Thomas Nevolianis"
__all__ = ["TSconversion"]

import pathlib


class TSconversion:
    """
    It converts the transition state conformers (xyz) to gjf gaussian format files at b3lyp/3-21g level.

    max_conformers: int : maximum number of transition state conformers to be taken from the "crest_conformers.xyz"
    crest_conformers: xyz file : transition state conformers
    charge : int : charge
    multiplicity: int : multiplicity of the transitions state
    gaussian_processors : int : gaussian processors
    """

    def __init__(self, charge, multiplicity, max_conformers, gaussian_processors, dft_level, ts):
        self.charge = charge
        self.multiplicity = multiplicity
        self.max_conformers = max_conformers
        self.gaussian_processors = gaussian_processors
        self.dft_level = dft_level
        self.ts = ts
        self.ts_name = pathlib.Path(ts).stem
        self.crest_conformers = self.create_ts_dir() / "crest_conformers.xyz"

    def create_ts_dir(self):
        """
        Creates the TS folder in which the calculations run.
        """
        tmp_dir = pathlib.Path(".", self.ts_name)
        tmp_dir.mkdir(exist_ok=True, parents=True)  # it's fine if it already exists
        return tmp_dir

    def write_gjf_file(self, file_name, atoms_number, conformer, multiplicity):
        """
        Writes the transition state conformer in gjf format file.

        file_name: crest transition state conformers
        conformer: transition state conformer
        """
        coords = conformer["atoms"]
        with open(self.create_ts_dir() / file_name, "w") as f:
            f.write('%chk=' + 'test' + '.chk\n')
            f.write(f'%nprocshared={self.gaussian_processors}\n')
            f.write('%mem=48GB\n')
            f.write(f'# opt=(calcfc,ts,noeigentest) freq {self.dft_level}\n')
            f.writelines('\nTitle Card Required\n\n')
            f.write(str(self.charge) + ' ' + str(self.multiplicity) + '\n')
            for coord in coords:
                f.write(coord)
                f.write("\n")
            f.write('\n\n')

    def build_atom_list(self, open_file, num_of_atoms):
        """
        Builds the atom list.

        open_file: transition state conformer file
        num_of_atoms: int : number of transition state conformer atoms
        """
        comment = open_file.readline().rstrip()
        atoms = []
        while num_of_atoms:
            atoms.append(open_file.readline().strip())
            num_of_atoms -= 1
        return {"atoms": atoms, "comment": comment}

    def build_conformer_list(self, xyz_file, max_num_conformers):
        """
        Builds the transition state conformer list.

        param xyz_file: xyz file : crest transition state conformer
        max_num_conformers: int : maximum number of conformers
        """
        with open(xyz_file) as f:
            conformers = []
            while True:
                if len(conformers) >= max_num_conformers:
                    break
                try:
                    num_of_atoms = int(f.readline())
                except:
                    break
                conformers.append(self.build_atom_list(f, num_of_atoms))
        return conformers

    def crest2gjf(self):
        """
        Converts the crest ransition state conformers to gjf Gaussian format.
        """
        conformers = self.build_conformer_list(self.crest_conformers, self.max_conformers)
        for i, conformer in enumerate(conformers):
            self.write_gjf_file(f"conformer{i}.gjf", len(conformer["atoms"]), conformer, self.multiplicity)
