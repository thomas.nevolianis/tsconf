# This source code is part of the TSconf package and is distributed
# under the 3-Clause BSD License. Please see 'LICENSE.rst' for further
# information.

__name__ = "ts_conf"
__author__ = "Thomas Nevolianis"
__all__ = ["TSConf"]

import pathlib
import os
from shutil import copy2
import time
import subprocess


class TSConf:
    """
    A parser to CREST system that is used for the conformational search of transition states. Transition state
    conformers generated at the GFN2-xTB level.
    CREST documentation:
    https://xtb-docs.readthedocs.io/en/latest/crest.html

    ts : xyz format file : transition state
    atoms : int : fixed active atoms
    multiplicity : int : multiplicity
    crest_processors : int : number of processors to run crest
    """

    def __init__(self, ts, charge, atoms, multiplicity, crest_processors, rwth_project):
        self.ts = ts
        self.ts_name = pathlib.Path(ts).stem
        self.charge = charge
        self.atoms = ','.join(str(e) for e in atoms)
        self.multiplicity = multiplicity
        self.crest_processors = crest_processors
        self.rwth_project = rwth_project

    def create_ts_dir(self):
        """
        Creates the TS folder in which the calculations run.
        """
        tmp_dir = pathlib.Path(".", self.ts_name)
        tmp_dir.mkdir(exist_ok=True, parents=True)  # it's fine if it already exists
        return tmp_dir

    def create_crest_tmp(self):
        """
        Creates a temporary CREST folder in which the crest calculations run.
        """
        tmp_dir = pathlib.Path(".", self.create_ts_dir() / "crest_tmp")
        tmp_dir.mkdir(exist_ok=True, parents=True)  # it's fine if it already exists
        return tmp_dir

    def copy_ts_to_cwdir(self, destination: pathlib.Path = None):
        """
        Copies the transition state conformer to the current working directory 'destination: pathlib.Path = None'.
        """
        if destination is None:
            destination = self.create_crest_tmp()
        copy2(pathlib.Path(self.ts), destination)

    def apply_constrain(self, work_directory: pathlib.Path = None):
        """
        It applies the constrain to atoms before starting the crest calculations.
        """
        if work_directory is None:
            work_directory = self.create_crest_tmp()
        try:
            p = subprocess.run(f"crest {self.ts} --constrain {self.atoms}",
                               cwd=f'{work_directory}',
                               check=True,
                               shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            raise ValueError(f"CREST constrain failed with errorcode {e.returncode}"
                             f" and stderr: {e.stderr}")
        else:
            with open(work_directory / 'crest_constrain.stdout', 'w') as out:
                out.write(p.stdout.decode())

    def change_force_constant(self, work_directory: pathlib.Path = None):
        """
        Creates the 'constrain.input' with a large force constant.
        """
        if work_directory is None:
            work_directory = self.create_crest_tmp()

        output_file = work_directory / 'constrain.input'
        with open(work_directory / '.xcontrol.sample', "rt") as fin:
            with open(output_file, "wt") as fout:
                for line in fin:
                    fout.write(line.replace('0.5', '1.0'))

    def create_job_file(self, file_path: pathlib.Path = None):
        """
        It creates the job bash file that is used with SLURM to submit the crest calculations.

        CREST settings:
        -coord : transition state file
        -uhf : multiplicity
        -chrg : charge
        -cinp : keyword for constrain input file
        -constrain.input : constraint input file
        -gfn2 : GFN2-xTB level
        -T 32 : uses 32 processors
        """
        if file_path is None:
            file_path = self.create_crest_tmp() / "job_crest.sh"

        with open(file_path, 'w') as f2:
            f2.write('#!/bin/bash\n')
            f2.write('#SBATCH -J crest_search\n')
            f2.write('#SBATCH -e log.err.%j\n')
            f2.write('#SBATCH -o log.out.%j\n')
            f2.write('#SBATCH -t 20:00:00\n')
            f2.write(f'#SBATCH -n {self.crest_processors}\n')
            f2.write('#SBATCH --mem=48G\n')
            f2.write(f'#SBATCH --account={self.rwth_project}\n\n')
            f2.write('module load CHEMISTRY xtb\n')
            f2.write(f"crest coord -cinp constrain.input -gfn2 -T "
                     f"{self.crest_processors} --uhf {self.multiplicity} -chrg {self.charge}")

    def run_crest(self):
        """
        Runs and checks if that CREST worked correctly at GFN2-xTB level.
        CREST settings:
        -coord : transition state file
        -uhf : multiplicity
        -chrg : charge
        -cinp : keyword for constrain input file
        -constrain.input : constraint input file
        -gfn2 : GFN2-xTB level
        -T 32 : uses 32 processors
        """
        print(f"Searching for TS conformers...\n" + "Grab a coffee and come back in a while\n")
        work_directory = self.create_crest_tmp()
        self.copy_ts_to_cwdir(work_directory)
        self.apply_constrain(work_directory)
        self.change_force_constant(work_directory)
        job_script_path = work_directory / "job_crest.sh"
        self.create_job_file(job_script_path)
        try:
            p = subprocess.run(f"sbatch < job_crest.sh",
                               cwd=f'{work_directory}',
                               check=True,
                               shell=True,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        except subprocess.CalledProcessError as e:
            raise ValueError(f"CREST failed with errorcode {e.returncode}"
                             f" and stderr: {e.stderr}")
        else:
            with open(work_directory / 'crest.stdout', 'w') as out:
                out.write(p.stdout.decode())
        self.check_crest_finished()
        self.copy_crest_to_dir(work_directory)

    def check_crest_finished(self, work_directory: pathlib.Path = None):
        """
        Waits if the jobs from crest finished by checking if the "crest_conformers.xyz" exists.
        """
        if work_directory is None:
            work_directory = self.create_crest_tmp()
        crest_conformers = work_directory / "crest_conformers.xyz"
        time_to_wait = 10000  # --> 28 hours
        time_counter = 0
        while not os.path.exists(crest_conformers):
            time.sleep(10)
            time_counter += 1
            if time_counter > time_to_wait: break

    def copy_crest_to_dir(self, work_directory: pathlib.Path = None):
        """
        Copies the transition state conformer to the main working directory 'file_path: pathlib.Path = None'.
        """
        if work_directory is None:
            work_directory = self.create_crest_tmp()
        file_path = work_directory / "crest_conformers.xyz"
        copy2(file_path, self.create_ts_dir())
