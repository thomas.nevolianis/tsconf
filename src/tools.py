# This source code is part of the TSconf package and is distributed
# under the 3-Clause BSD License. Please see 'LICENSE.rst' for further
# information.

__name__ = "tools"
__author__ = "Thomas Nevolianis"
__all__ = ["get_multiplicity"]

from openbabel import openbabel


def get_multiplicity(ts_xyz):
    """
    It returns the multiplicity of a given xyz file.

    ts_xyz: str : transition state file

    :return : int : multiplicity
    """
    obconversion = openbabel.OBConversion()
    obconversion.SetInFormat("xyz")
    obmol = openbabel.OBMol()
    obconversion.ReadFile(obmol, ts_xyz)
    multiplicity = obmol.GetTotalSpinMultiplicity()

    return multiplicity

