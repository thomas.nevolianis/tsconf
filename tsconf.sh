#!/bin/bash
echo "Running TSconf installation..."
python3 setup.py install --user
echo "Finished."
echo "Running TSconf..."
module load CHEMISTRY openbabel/3.1.1
clear
python3 main.py
